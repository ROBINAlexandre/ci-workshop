@extends('layouts.app')

@section('page')
Home page
@endsection

@section('content')
<style>
</style>
<div class="cointainer-fluid">
    <div class="card-view , animated  bounceIn, color-hep">
        <div class="card-panel">
            <h3 class="color-text-hep"> Bonjour, vous voici sur le parcours étudiant du campus HEP de Nantes </h3>
            <p class="color-text-hep"> Ce parcours est parcours d'information personnalisé pour répondre aux questions des étudiants rapidement. A la fin vous pourrez .... </p>
        </div>
    </div>
</div>
<div class="cointainer-fluid" >
    <div class="card-view">
    <div class="row">
        <div class="col s12">
        <div class="pp">    
            <div class="progress-bar "></div>
        </div>
        </div>
    </div> 
    <div class="row" id="stepRow">

    </div>
    </div>

    <button style="display:none" id="toggleModal" type="button" data-toggle="modal" data-target="#infos" class="btn btn-primary"></button>
        <div class="modal" id="infos">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Plus d'informations</h4>
                </div>
            <div class="modal-body">
                 Mes felicitations. Vous avez finis votre parcours. Suites à vos réponse nous pouvons vous conseillez l'école suivante : 
                <img src="{{ asset('img/epsi.png') }}" class="img-frame img-pointer modal-trigger">
                <p>Retrouver l'EPSI sur <a href="http://www.epsi.fr/" target="_blank">www.epsi.fr/</a></p>
            </div>
            <div class="modal-footer">
        </div>
    
        </div>
    </div>
</div>
        

@endsection