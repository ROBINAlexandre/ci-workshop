
  @extends('layouts.app')

  @section('page')
  Home page
  @endsection

  @section('content')
    <div class="cointainer-fluid">
      <div class="card-view">
        <div class="row">
            <div class="col s12">
              <ul class="tabs">
                <li class="tab col s2"><a class="active" href="#test1">MyDil</a></li>
                <li class="tab col s2"><a href="#test2">Accélérateur Idrac</a></li>
                <li class="tab col s2"><a href="#test3">IFAGTORY</a></li>
                <li class="tab col s2"><a href="#test4">SALLE DE PAUSE</a></li>
                <li class="tab col s2"><a href="#test5">OPEN-SPACE MODULABLE</a></li>
                <li class="tab col s2"><a href="#test6">SALLE DE RECEPTION</a></li>
              </ul>
            </div>
            <div id="test1" class="col s12">
              <h4>MyDil</h4>
              <div id="promo-notifications" class="color-hep">
                <ul>
                  <li>Lieu de formation et d’initiation aux outils informatiques et numériques.</li>
                  <li>Lieu de rendez-vous pour les GEEK.</li>
                  <li>Lieu de réflexion et d’expérimentation des nouveaux usages.</li>
                </ul>
              </div>



<div class="row">
  <div class="col s12">
    <img src="{{asset('img/mydil.png')}}" style="width:100%;">
  </div>
  <div class="col s6">
    <table class="responsive-table striped">
      <thead>
        <tr>
          <th><h5><b>Technologie disponible</b></h5></th>
        </tr>
      </thead>

      <tbody>
        <tr><td>&#9658; NAO, le robot humanoïde sur lequel vous inventez le compagnon de demain.</td></tr>
        <tr><td>&#9658; L’oculus rift et sa nouvelle forme d’immersion en 3D.</td></tr>
        <tr><td>&#9658; Les nouvelles technologies de réalité augmentée (hololens…).</td></tr>
        <tr><td>&#9658; L’impression 3D pour le prototypage.</td></tr>
        <tr><td>&#9658; Les objets connectés d’aujourd’hui, et surtout ceux de demain, bracelets, montres, balance tissu…</td></tr>
      </tbody>
    </table>
  </div>
  <div class="col s6">
    <table class="responsive-table striped">
      <thead>
        <tr>
          <th><b><br><br></b></th>
        </tr>
      </thead>

      <tbody>
        <tr><td>&#9658;Les nouvelles interfaces homme/machine, comme le leap motion.</td></tr>
        <tr><td>&#9658; Les différentes formes de tablettes et Smartphones nécessaires au développement des applications du futur.</td></tr>
        <tr><td>&#9658;Les technologies de système embarqué (raspberry py, arduino…).</td></tr>
        <tr><td>&#9658;Les drônes et leurs nouvelles applications.</td></tr>
        <tr><td>&#9658; Des outils collaboratifs innovants comme la surface hub.</td></tr>
      </tbody>
    </table>
  </div>
</div>
<ul class="accordion--home">
  	<li class="slide" style="background-image: url('{{asset('img/nao.jpg')}}');">
      <a class="image--content" target="_blank">
        <h4>Robot NAO</h4>
      </a>
	  </li>

  	<li class="slide" style="background-image: url('{{asset('img/oculus.jpg')}}');">
      <a class="image--content" target="_blank">
        <h4>Technologie VR</h4>
      </a>
	  </li>

  	<li class="slide" style="background-image: url('{{asset('img/rasp.png')}}');">
      <a class="image--content" target="_blank">
        <h4>Raspberry PI</h4>
      </a>
	  </li>

  <li class="slide" style="background-image: url('{{asset('img/impr.jpeg')}}');">
    <a class="image--content" target="_blank">
      <h4>Impression 3D</h4>
    </a>

    		</a>
  	</li>
</ul>
            </div>
            <div id="test2" class="col s12">
              <h4>Accélérateur Idrac</h4>
<div class="row">
<div class="col s6">
<table class="responsive-table striped">
<thead>
<tr>
<th><b> L’accompagnement se traduira également au quotidien par :</b></th>
</tr>
</thead>

<tbody>
  <tr><td> ► Des conditions tarifaires avantageuses sur les services logistiques,</tr></td>
  <tr><td> ► Un suivi de l’évolution des projets et des phases de croissance,</tr></td>
  <tr><td> ► Des échanges facilités pour accélérer le partage de connaissances et d’expériences,</tr></td>
  <tr><td> ► Une communauté apprenante et bienveillante réunie autour de nos incubés,</tr></td>
  <tr><td> ► Un accompagnement à la recherche de financement et à la levée de fonds... </tr></td>

</tbody>
</table>
</div>
<div class="col s6">

  <table class="responsive-table striped">
  <thead>
  <tr>
  <th><b>L’incubateur du Campus HEP NANTES propose, à titre gracieux, aux porteurs de projet sélectionnés (pour 6 mois renouvelable 1 fois) :</b></th>
  </tr>
  </thead>

  <tbody>
    <tr><td> ► Un accompagnement individuel et sur-mesure : accompagnement stratégique et logistique, mentorat...</td></tr>
    <tr><td> ► Un accompagnement collectif : workshop et ateliers thématiques, témoignages d’entrepreneurs, évènements réseaux... </td></tr>

  </tbody>
  </table>
</div>
</div>
<ul class="accordion--home">
<li class="slide" style="background-image: url('{{asset('img/gdp.jpg')}}');">
<a class="image--content" target="_blank">
<h4>Gestion de Projet</h4>
</a>
</li>

<li class="slide" style="background-image: url('{{asset('img/comp.jpg')}}');">
<a class="image--content" target="_blank">
<h4>Transmission de compétence</h4>
</a>
</li>

<li class="slide" style="background-image: url('{{asset('img/log.jpg')}}');">
<a class="image--content" target="_blank">
<h4>Aide logistique</h4>
</a>
</li>

<li class="slide" style="background-image: url('{{asset('img/fin.jpg')}}');">
<a class="image--content" target="_blank">
<h4>Recherche de financement</h4>
</a>

</a>
</li>
</ul>
            </div>
            <div id="test3" class="col s12">
              <h4>IFAGTORY</h4>
              <div class="row">
              <div class="col s6">
              <table class="responsive-table striped">
              <thead>
              <tr>
                <th><b>NOS OBJECTIFS</b></th>
              </tr>
              </thead>

              <tbody>
                <tr><td>Préparer et former de futurs entrepreneurs, repreneurs & des intrapreneurs</tr></td>
                <tr><td>Créer et sauvegarder les emplois de demain</tr></td>
                <tr><td>Maximiser le taux de survie à 3 ans des entreprises hébergées</tr></td>
              </tbody>
              </table>
              </div>
              <div class="col s6">
              <table class="responsive-table striped">
              <thead>
              <tr>
              <th><b>NOTRE PHILOSOPHIE</b></th>
              </tr>
              </thead>

              <tbody>
                <tr><td>Accompagner tout profil souhaitant entreprendre</td></tr>
                <tr><td>Héberger tous les types de projets dans des lieux stimulants</tr></td>
                <tr><td>Proposer des formations utiles et personnalisées</tr></td>
                <tr><td>Stimuler la cohésion</tr></td>
                <tr><td>Accélérer la croissance des entreprises créées</tr></td>
                <tr><td>Maximiser le taux de survie des entreprises accompagnées</tr></td>

              </tbody>
              </table>
              </div>
              <br><hr><br>
              <div class="col s12">

                <table class="responsive-table striped">
                <thead>
                <tr>
                  <th><b>UNE FORMATION INNOVANTE</b></th>
                  <th><b>UN ACCOMPAGNEMENT DYNAMIQUE</b></th>
                  <th><b>UN ESPACE DÉDIÉ</b></th>
                </tr>
                </thead>

                <tbody>
                  <tr>
                    <td>Accès à une multitude d’évènements et de mises en situation (Conférences, Open Innovation Bootcamp, Mash-up session, crash-test session, pitch-party, etc…)</td>
                    <td>Suivi par un mentor et conseils avisés selon les besoins (finance, marketing, commercial, RH, juridique, etc…)</td>
                    <td>Un mobilier design et adapté au travail collaboratif et créatif</td>
                  </tr>
                </tbody>
                </table>

              </div>
              </div>
              <ul class="accordion--home">
              <li class="slide" style="background-image: url('{{asset('img/jent.jpg')}}');">
              <a class="image--content" target="_blank">
              <h4>Accompagnement</h4>
              </a>
              </li>

              <li class="slide" style="background-image: url('{{asset('img/pitch.jpg')}}');">
              <a class="image--content" target="_blank">
              <h4>Pitch-Party</h4>
              </a>
              </li>

              <li class="slide" style="background-image: url('{{asset('img/coh.jpg')}}');">
              <a class="image--content" target="_blank">
              <h4>Cohésion de groupe</h4>
              </a>
              </li>

              <li class="slide" style="background-image: url('{{asset('img/strat.jpg')}}');">
              <a class="image--content" target="_blank">
              <h4>Accompagnement stratégique</h4>
              </a>

              </a>
              </li>
              </ul>


            </div>
          <div id="test4" class="col s12">
            <h4>SALLE DE PAUSE</h4>

            <ul class="accordion--home center">
            <li class="slide" style="background-image: url('{{asset('img/p1.jpg')}}');">
            <a class="image--content" target="_blank">
            <h4>1er Étage</h4>
            </a>
            </li>

            <li class="slide" style="background-image: url('{{asset('img/p2.jpg')}}');">
            <a class="image--content" target="_blank">
            <h4>2 Étage</h4>
            </a>
            </li>

            <li class="slide" style="background-image: url('{{asset('img/p3.jpg')}}');">
            <a class="image--content" target="_blank">
            <h4>3 Étage</h4>
            </a>
            </li>

            <li class="slide" style="background-image: url('{{asset('img/p.jpg')}}');">
            <a class="image--content" target="_blank">
            <h4>Envie d'une pause</h4>
            </a>
            </li>
            </a>
            </li>
            </ul>
          </div>

          <div id="test5" class="col s12">
            <h4>OPEN-SPACE MODULABLE</h4>
            <ul class="accordion--home center">
            <li class="slide" style="background-image: url('{{asset('img/op1.jpg')}}');">
            <a class="image--content" target="_blank">
            <h4>1er Étage</h4>
            </a>
            </li>

            <li class="slide" style="background-image: url('{{asset('img/op2.png')}}');">
            <a class="image--content" target="_blank">
            <h4>2 Étage</h4>
            </a>
            </li>

            <li class="slide" style="background-image: url('{{asset('img/op3.jpg')}}');">
            <a class="image--content" target="_blank">
            <h4>3 Étage</h4>
            </a>
            </li>

            <li class="slide" style="background-image: url('{{asset('img/op3.PNG')}}');">
            <a class="image--content" target="_blank">
            <h4>Co-Working</h4>
            </a>
            </li>
            </a>
            </li>
            </ul>
          </div>
          <div id="test6" class="col s12">
            <h4>SALLE DE RECEPTION</h4>
              <img src="{{asset('img/sdr.png')}}" style="width:100%;">
          </div>

        </div>
      </div>
    </div>
  @endsection
