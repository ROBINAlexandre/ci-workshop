@extends('layouts.app')

@section('page')
Home page
@endsection

@section('content')
<div class="container-fluid">
  <div class="card-view">
    <div id="promo-notifications" class="color-hep">
      <ul>
        <li>Bienvenue sur le campus HEP NANTES -*- Cliquez sur un marqueur pour en savoir plus !</li>
        <li>Bienvenue sur le campus HEP NANTES -*- Cliquez sur un marqueur pour en savoir plus !</li>
      </ul>
    </div>
    <div class="row">
      <div class="col s8">
        <img src="{{ asset('img/campus.jpg') }}"class="img-campus">
        <img src="{{ asset('img/mark-ceb.png') }}" class="img-mark img-pointer animated infinite bounce delay-2s markHEP" id="ceb" style="left: 4%;top: 8%;">
        <img src="{{ asset('img/mark-ecfo.png') }}" class="img-mark img-pointer animated infinite bounce delay-2s markHEP"id="ecfo" style="left:44%;top:8%">
        <img src="{{ asset('img/mark-env.png') }}" class="img-mark img-pointer animated infinite bounce delay-2s markHEP"id="env"style="left:20%;top:18%;">
        <img src="{{ asset('img/mark-hepnv.png') }}" class="img-mark img-pointer animated infinite bounce delay-2s markHEP"id="hepnv"style="left:38%;top:26%;">
        <img src="{{ asset('img/mark-inc.png') }}" class="img-mark img-pointer animated infinite bounce delay-2s markHEP"id="inc"style="left:10%;top:30%;">
  </div>
        <div class="col s4">
          <div class="hepnv text-here">
            <h3>HEP : nos valeurs</h3>
            <p>Humanisme, Entrepreunariat et Professionnalisme</p>
            <p>Les écoles du campus sont engagées dans une démarche contre le racisme et l'antisémitisme. Elles portent également une attention particulière aux cas de cyberviolence sur les réseaux sociaux</p>
            <p>Les écoles proposent différents accompagnement aux entrepreneurs pour la création ou la reprise d'entreprise</p>
            <p>Les écoles sont reconnues par les entreprises pour leur savoir-faire en matière d'apprentissage et de formation</p>
          </div>
          <div class="ceb text-here">
            <h3>Le campus en bref</h3>
            <p>Le campus dispose de nombreuses salles modulables, des espaces de wo-working, des incubateurs d'entreprises, des learnings labs.</p>
            <p>Ces espaces sont répartit dans un batiment de 4 000m² imaginé par l'architecte Rudy Ricciotti</p>
            <p>le CAMPUS HEP Nantes se singularise également par son parti-pris environnemental : équipements permettant une forte réduction des consommations énergétiques, forte continuité de l’espace végétal, grande terrasse paysagée, confort
              acoustique performant.</p>
          </div>
          <div class="env text-here">
            <h3>L'environnement</h3>
            <p>En plein coeur de l'Îles de Nantes, le campus est idéalement situé près du centre commercial Beaulieu, de la cité internationale des congrès et des machines de l'île de Nantes</p>
            <hr>
            <img src="{{ asset('img/map_nantes.png') }}" class="img-campus" width="100%" height="100%">
  </div>
            <div class="ecfo text-here">
              <h3>Des écoles, des formations</h3>
              <p>7 écoles présentes</p>
              <p>17 programmes de formation de BAC à BAC+5</p>
              <p>13 filières métier différentes : </p>
              <ul>
                <li>&#9658; Commerce & Marketing</li>
                <li>&#9658; Communication</li>
                <li>&#9658; Gestion & études financières</li>
                <li>&#9658; Ingénierie informatique</li>
                <li>&#9658; Logiciels libres & solutions open source</li>
                <li>&#9658; Management & entrepreneuriat</li>
                <li>&#9658; Métiers du web</li>
              </ul>
            </div>
            <div class="inc text-here">
              <h3>Des incubateurs</h3>
              <ul>
                <li>&#9658; <b>Le labo MyDIL</b></li>
                <p>Laboratoire dédié au codage et à l’innovation technologique de l’école EPSI et lieu de collaboration proposant du matériel modulable et high-tech, MyDIL est à la portée des élèves pour échanger, développer, accélérer et favoriser
                  l’émergence de projets innovants</p>
                <li>&#9658; <b>IFAGTORY</b></li>
                <p>Réseau des 16 incubateurs de l’école IFAG présent sur l’ensemble du territoire national, IFAGTORY accompagne tous ses étudiants inscrits dans le parcours entrepreneurs et tous les entrepreneurs souhaitant bénéficier d’un
                  accompagnement, d’un hébergement à la création ou de la reprise d’entreprise.</p>
                <li>&#9658; <b>Accélérateur IDRAC</b></li>
                <p>Structure d’accompagnement centrée sur le « crash-test » des projets start-up et sur la volonté de questionner les modèles existants des entreprises, l’Accélérateur IDRAC Business School vient en complément ou à la suite d’un
                  passage en incubateur.</p>
              </ul>
            </div>
          </div>



        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="card-view">
        <div class="row">
          <div class="col s1"></div>
          <div class="col s3">
            <div class="square animated infinite tada delay-5s">
              <img src="{{ asset('img/epsi.png') }}" class="img-frame img-pointer modal-trigger" href="#epsi">
      </div>
            </div>
            <div class="col s1"></div>
            <div class="col s2">
              <div class="square animated infinite tada delay-5s">
                <img src="{{ asset('img/idrac.png') }}"class="img-frame img-pointer modal-trigger" href="#idrac">
      </div>
              </div>
              <div class="col s1"></div>
              <div class="col s1"></div>
              <div class="col s3">
                <div class="square animated infinite tada delay-5s">
                  <img src="{{ asset('img/igefi.png') }}"class="img-frame img-pointer modal-trigger" href="#igefi">
      </div>
                </div>
              </div>
              <div class="row">
                <div class="col s5"></div>
                <div class="col s2">
                  <div class="square animated infinite tada delay-5s">
                    <img src="{{ asset('img/ifag.png') }}"class="img-frame img-pointer modal-trigger" href="#ifag">
      </div>
                  </div>
                  <div class="col s5"></div>
                </div>
                <div class="row">
                  <div class="col s1"></div>
                  <div class="col s3">
                    <div class="square animated infinite tada delay-5s">
                      <img src="{{ asset('img/opss.png') }}"class="img-frame img-pointer modal-trigger" href="#opss">
      </div>
                    </div>
                    <div class="col s1"></div>
                    <div class="col s2">
                      <div class="square animated infinite tada delay-5s">
                        <img src="{{ asset('img/sup.png') }}"class="img-frame img-pointer modal-trigger" href="#sup">
      </div>
                      </div>
                      <div class="col s1"></div>
                      <div class="col s1"></div>
                      <div class="col s3">
                        <div class="square animated infinite tada delay-5s">
                          <img src="{{ asset('img/wis.png') }}"class="img-frame img-pointer modal-trigger" href="#wis">
      </div>
                        </div>
                      </div>
                    </div>
                  </div>

                    <div class="container-fluid">
                      <div class="card-view">
                        <div class="row">
                          <div class="col s6">
                            <div id='map'></div>
                          </div>
                          <div class="col s3 center margin-qr">
                            <div style="border-left:3px solid #000;border-right:3px solid #000;height:100%">
                              <h2> Trouvez mon chemin </h2>
                <img src="{{asset("img/qr-carte.gif")}}">
</div>
              </div>
              <div class="col s3">

                <img src="{{asset("img/campus-carte.jpg")}}" style="width:98%;height: 500px;">
</div>
              </div>
            </div>
          </div>



                  <!-- Modal Structure -->
                  <div id="epsi" class="modal modal-overflow">
                    <div class="section">
                      <div class="container-fluid">
                        <div class="card-view">
                          <div class="row">
                            <div class="col s6">

                              <h2 class="center">EPSI</h3>
                                <p>Retrouver l'EPSI sur <a href="http://www.epsi.fr/" target="_blank">www.epsi.fr</a></p>
                            <hr>
                          <h4  > INFORMATIONS GÉNÉRALES </h4>
                          <p>&#9658; Présents dans plus de 9 villes</p>
                          <p>&#9658; 5000 entreprises partenaire</p>
                          <p>&#9658; Plus de 2000 étudiants</p>
                          <p>&#9658; Plus de 30 000 diplomés</p>

                          <h4  > CERTIFICATION </h4>
                          <h5> Titre RNCP de niveau 2 :  </h5>
                          <p>&#9658; Concepteur Développeur Informatique (CDI)</p>
                          <p>&#9658; Administrateur Systèmes et Réseaux et BDD (ASRBD)</p>
                          <h5> Titre RNCP de niveau 1 :  </h5>
                          <p>&#9658; Expert en Informatique et Systèmes Informatiques (EISI)</p>
                        </div>
                        <div class="col s6 center">
                          <h4> FORMATIONS </h4>

                          <img src="{{ asset('img/epsi-formations.png') }}">
                          </div>
                          </div>
                          </div>
                          </div>
                          <div class="modal-footer">
                            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Fermer</a>
                          </div>
                        </div>
                    </div>

                    <!-- Modal Structure -->
                    <div id="idrac" class="modal modal-overflow">
                      <div class="section">
                        <div class="container-fluid">
                          <div class="card-view">
                            <div class="row">
                              <div class="col s12">

                                <h2 class="center">IDRAC</h3>
                                <p>Retrouver l'IDRAC sur <a href="http://www.ecoles-idrac.com//" target="_blank">www.ecoles-idrac.com</a></p>
                              <hr>
                            <h4  > INFORMATIONS GÉNÉRALES </h4>
                            <p>Présent dans 10 villes</p>
                            <p>Présent sur 6 campus à l'international :</p>
                            <ul>
                              <li>&#9658; Dublin et Cork en Irlande</li>
                              <li>&#9658; Barcelone et Santander en Espagne</li>
                              <li>&#9658; New York aux Etats-Unis</li>
                              <li>&#9658; Brno en Réplublique Tchèque</li>
                            </ul>

                          </div>
                          <div class="col s12 center">
                            <h4> FORMATIONS </h4>
                            <div class="row">
                              <div class="col s4">
                                <img src="{{ asset('img/programme-idrac1.png') }}" height="100%" width="100%">
                              </div>
                              <div class="col s4">
                                <img src="{{ asset('img/programme-idrac2.png') }}" height="100%" width="100%">
                              </div>
                              <div class="col s4">
                                <img src="{{ asset('img/programme-idrac3.png') }}" height="100%" width="100%">
                              </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="modal-footer">
                              <a href="#!" class="modal-close waves-effect waves-green btn-flat">Fermer</a>
                            </div>
                          </div>
                      </div>

                      <!-- Modal Structure -->
                      <div id="igefi" class="modal modal-overflow">
                        <div class="section">
                          <div class="container-fluid">
                            <div class="card-view">
                              <div class="row">
                                <div class="col s12">

                                  <h2 class="center">IGEFI</h2>
                                  <p>Retrouver IGEFI sur <a href="http://www.igefi.net/" target="_blank">www.igefi.net</a></p>
                                <hr>
                              <h4>Informations Générales</h4>
                              <p>Présent dans 3 villes :</p>
                              <ul>
                                <li>&#9658; Nantes</li>
                                <li>&#9658; Paris</li>
                                <li>&#9658; Lyon</li>
                              </ul>
                            </div>
                            <div class="col s12 center">
                              <h4> FORMATIONS </h4>
                              <div class="row">
                                <div class="col s12">
                                  <img src="{{ asset('img/igefi-formations1.png') }}" height="100%" width="100%">
                                </div>
                                <div class="col s12">
                                  <img src="{{ asset('img/igefi-formations2.png') }}" height="100%" width="100%">
                                </div>
                                <div class="col s12">
                                  <img src="{{ asset('img/igefi-formations3.png') }}" height="100%" width="100%">
                                </div>
                              </div>
                              </div>
                              </div>
                              </div>
                              </div>
                              <div class="modal-footer">
                                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Fermer</a>
                              </div>
                            </div>
                        </div>

                        <!-- Modal Structure -->
                        <div id="ifag" class="modal modal-overflow">
                          <div class="section">
                            <div class="container-fluid">
                              <div class="card-view">
                                <div class="row">
                                  <div class="col s12">

                                    <h2 class="center">IFAG</h3>
                                    <p>Retrouver l'IFAG sur <a href="http://www.ifag.com//" target="_blank">www.ifag.com</a></p>
                                  <hr>
                                  <div class="row">
                                    <div class="col s6">
                                      <h4  > Informations Générales </h4>
                                      <p>École n°1 de l'entreprenariat</p>
                                      <p>Présente dans 16 villes</p>

                                      <h4  > CERTIFICATION </h4>
                                      <p>Titre RNCP de niveau 2 : </p>
                                      <p>&#9658; Responsable Opérationnel d'Activité</p>
                                      <p>Titre RNCP de niveau 1 : </p>
                                      <p>&#9658; Manager d'Entreprise ou de Centre de Profit</p>
                                    </div>
                                    <div class="col s6">
                                        <img src="{{ asset('img/ifag-formations_1.png') }}" height="100%" width="100%">
                                    </div>
                                  </div>
                                    </div>

                              </div>
                              <div class="col s12 center">
                                <h4> FORMATIONS </h4>
                                <div class="row">
                                  <div class="col s12">
                                    <img src="{{ asset('img/ifag-formations_2.png') }}" height="100%" width="100%">
                                  </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                <div class="modal-footer">
                                  <a href="#!" class="modal-close waves-effect waves-green btn-flat">Fermer</a>
                                </div>
                              </div>
                          </div>
                                                  <!-- Modal Structure -->
                                                  <div id="opss" class="modal modal-overflow">
                                                    <div class="section">
                                                      <div class="container-fluid">
                                                        <div class="card-view">
                                                          <div class="row">
                                                            <div class="col s12">

                                                              <h2 class="center">OPEN SOURCE SCHOOL</h2>
                                                              <p>Retrouver l'IDRAC sur <a href="http://www.opensourceschool.fr/" target="_blank">www.opensourceschool.fr</a></p>
                                                            <hr>
                                                            <div class="row">
                                                              <div class="col s6">
                                                                <h4  > Informations Générales </h4>
                                                                <p>Présent dans 6 villes :</p>
                                                                <ul>
                                                                  <li>&#9658; Nantes</li>
                                                                  <li>&#9658; Paris</li>
                                                                  <li>&#9658; Bordeaux</li>
                                                                  <li>&#9658; Lyon</li>
                                                                  <li>&#9658; Montpellier</li>
                                                                  <li>&#9658; Lille</li>
                                                                </ul>
                                                              </div>
                                                            </div>
                                                          </div>
                                                          </div>
                                                          </div>
                                                          <div class="modal-footer">
                                                            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Fermer</a>
                                                          </div>
                                                        </div>
                                                    </div>
                                                    </div>

                                                    <!-- Modal Structure -->
                                                    <div id="sup" class="modal modal-overflow">
                                                      <div class="section">
                                                        <div class="container-fluid">
                                                          <div class="card-view">
                                                            <div class="row">
                                                              <div class="col s12">

                                                                <h2 class="center">SUP DE COM</h3>
                                                              <hr>

                                                            <h4>Informations Générales</h4>
                                                            <p>Présent dans 10 villes</p>
                                                            <p>1300 étudiants</p>
                                                            <p>500 professeurs</p>
                                                            <p>1500 entreprises partenaires</p>
                                                            <p>8 écoles à l'international</p>
                                                            <p>10 langues enseignées</p>
                                                          </div>
                                                          <div class="col s12 center">
                                                            <h4> FORMATIONS </h4>
                                                            <div class="row">
                                                              <div class="col s6">
                                                                <img src="{{ asset('img/sup-formations_1.jpg') }}" height="80%" width="80%">
                                                              </div>
                                                              <div class="col s6">
                                                                <img src="{{ asset('img/sup-formations_2.jpg') }}" height="80%" width="80%">
                                                              </div>
                                                            </div>
                                                            </div>
                                                            </div>
                                                            </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                              <a href="#!" class="modal-close waves-effect waves-green btn-flat">Fermer</a>
                                                            </div>
                                                          </div>
                                                      </div>


                            <!-- Modal Structure -->
                            <div id="wis" class="modal modal-overflow">
                              <div class="section">
                                <div class="container-fluid">
                                  <div class="card-view">
                                    <div class="row">
                                      <div class="col s12">

                                        <h2 class="center">WIS</h3>
                                      <hr>
                                      <div class="row">
                                        <div class="col s6">

                                          <h4>Informations Générales</h4>
                                          <p>Présent dans 5 villes</p>
                                          <ul>
                                            <li>&#9658; Nantes</li>
                                            <li>&#9658; Bordeaux</li>
                                            <li>&#9658; Montpellier</li>
                                            <li>&#9658; Lille</li>
                                            <li>&#9658; Lyon</li>
                                          </ul>
                                          <h4  > CERTIFICATION BAC+5 </h4>
                                          <p>&#9658; Titre RNCP certifié de Niveau I «Expert en Informatique et Système d’information»délivré par l’EPSI et publié au Journal Officiel du 07/06/2016</p>
                                          <p>&#9658; Titre RNCP certifié de Niveau I «Manager d’Entreprise ou de Centre de Profit»délivré par l’IFAG et publié au Journal Officiel du 18/12/2016</p>
                                        </div>
                                        <div class="col s6">
                                          <h4  > CERTIFICATION BAC+3 </h4>
                                          <p>Titre WIS – « Chef de projet digital »</p>
                                          <p>&#9658; Parcours métier Conception-Intégration de contenus Web</p>
                                          <p>Titre RNCP certifié de niveau II « Responsable de communication », délivré par AIPF (Association Internationale Pour la Formation) publié au Journal Officiel du 21 décembre 2017</p>
                                          <p>&#9658; Parcours métier Web Marketing</p>
                                          <p>Titre RNCP certifié de niveau II «Responsable management opérationnel commercial et marketing» délivré par AIPF (Association</p>
                                          <p>Internationale Pour la Formation) et publié au Journal Officiel du 18 Décembre 2016.<p>
                                            <p>&#9658; Parcours numérique Développement logiciel</p>
                                            <p>Titre RNCP certifié de niveau III «Développeur(se) Logiciel» publié au Journal Officiel du 19/02/2013000</p>
                                        </div>
                                      </div>

                                  </div>
                                  <div class="col s12 center">
                                    <h4> FORMATIONS </h4>
                                    <div class="row">
                                      <div class="col s6">
                                        <img src="{{ asset('img/wis-formations_1.jpg') }}" height="50%" width="50%">
                                      </div>
                                      <div class="col s6">
                                        <img src="{{ asset('img/wis-formations_2.jpg') }}" height="50%" width="50%">
                                      </div>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                    <div class="modal-footer">
                                      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Fermer</a>
                                    </div>
                                  </div>
                              </div>

                        @endsection
