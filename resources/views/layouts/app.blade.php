<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <title>Présentation campus HEP NANTES</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="shortcut icon" href="favicon.ico">
  <link href="{{ asset('css/animate.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('js/bootstrap/dist/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('css/materialize.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.49.0/mapbox-gl.css' rel='stylesheet' />


</head>

<body>
  <!-- Top Menu Items -->
<nav class="navbar navbar-inverse navbar-fixed-top color-hep">

  <div class="mobile-only-brand pull-right">
    <div class="nav-header  pull-left">
      <div class="nav-header  pull-right" style="width: 232%!important;">

        <div class="brand-absolute">
          Nombre de points accumulés : <a class="header-score">0</a>
          <a style="font-family: Andale Mono, monospace;font-size: -webkit-xxx-large;">Abso'LU</a>
        </div>
      </div>
  </div>
</div>
<div class="mobile-only-brand pull-left">
  <div class="nav-header  pull-left">
    <div class="logo-wrap">
      <a href="{{ url('/') }}">
        <img src="{{ asset('img/hep.png') }}" class="img-logo" style="width:92%">
      </a>
    </div>
  </div>
</div>
</nav>

  <div class="fixed-sidebar-left">
			<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;"><ul class="nav navbar-nav side-nav nicescroll-bar" style="overflow: hidden; width: auto; height: 100%;">
        <br>
				<li>
          <a href="./" class="active"><div class="pull-left"><i class="fas fa-home mr-20"></i><span class="right-nav-text">Acceuil</span></div><div class="clearfix"></div></a>
				</li>
				<li><hr class="light-grey-hr mb-10"></li>
        <li>
          <a href="./way"><div class="pull-left"><i class="fas fa-road mr-20"></i></i><span class="right-nav-text">Parcours découverte</span></div><div class="clearfix"></div></a>
				</li>
        <li><hr class="light-grey-hr mb-10"></li>
        <li>
          <a href="./visit"><div class="pull-left"><i class="fas fa-arrow-circle-right mr-20"></i><span class="right-nav-text">Visitez le campus</span></div><div class="clearfix"></div></a>
        </li>
        <li><hr class="light-grey-hr mb-10"></li>
        <li>
          <a href="./rss"><div class="pull-left"><i class="fas fa-rss mr-20"></i><span class="right-nav-text">Suivez l'actu de l'île</span></div><div class="clearfix"></div></a>
        </li>

		</div>
  </div>
  <div class="page-wrapper">

  @yield('content')
  <!-- Footer -->
  <footer class="footer container-fluid pl-30 pr-30">
    <div class="row">
      <div class="col-sm-12">
        <p>2018 &copy; Name by Abso'LU</p>
      </div>
    </div>

    <script src="{{ asset('js/materialize.js') }}"></script>
    <script src="{{ asset('js/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap/dist/js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/materialize.js') }}"></script>
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.49.0/mapbox-gl.js'></script>
    <script src="{{ asset('js/home.js') }}"></script>
    <script src="{{ asset('js/visit.js') }}"></script>
    <script src="{{ asset('js/way.js') }}"></script>
    <script src="{{ asset('js/bar.js') }}"></script>
    <script src="{{ asset('js/rss.js') }}"></script>

  </footer>
</div>

  <!-- /Footer -->
  </div>
  </div>
  <!-- /Main Content -->

  </div>
  </div>
  <!-- /#wrapper -->
  <!-- JavaScript -->


</body>

</html>
