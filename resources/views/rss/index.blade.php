@extends('layouts.app')

@section('page')
Home page
@endsection

@section('content')
<div class="cards row">

        <div class="container-fluid">
          <div class="card-view">
            <h4 class="color-text-hep">Fil d'actualité de l'île de Nantes</h4>
            <p>Voici le fil d'actualité de l'île de Nantes, petit bout de terre abrittant le campus. Chaque article proposer vous rapportera 5 points.</p> <p>Défiez vos amis tout en apprenant plus sur l'actu de l'île !! </p>
  <a class="btn btn-glory place-me"><i class="fas fa-trophy left"></i>5 points cadeau</a>
          </div>
        </div>


</div>
@endsection
