let progressTotal = 0;
 $(document).on('click','.choice', e => {
   // get the event on click and target the data attr from them
   const current = $('.choices.active');
   const currentDot = $('.dots.active');
   const parent = $(e.currentTarget).parents('.choices');
   const value = $(e.currentTarget).data('value');
   progressTotal = progressTotal + value;
   if (parent.hasClass('end')) {

     return '';
   }
   const next = current.data('next');
   currentDot.addClass('finished');

   // this is how i am adding the animation for the slider to go 50% or all the way to 100%.  //
   console.log(currentDot.data('previous'));
   if (currentDot.data('next') === 2 || currentDot.data('previous') === 3) {
     $('.progress').addClass('fifty');
     $('.dot2').addClass('fifty');
   } else if (currentDot.data('next') === 3) {
     $('.progress').addClass('onehundred');
     $('.dot3').addClass('onehundred');
   }
   // this is how i am going to the next current slide . //

   parent.addClass('finished');
   currentDot.removeClass('active');
   $(`.dots[data-current='${next}']`).addClass('active');

   return '';

 });

 $('.button-reset').on('click', e => {
   progressTotal = 0;

   $('.progress').removeClass('fifty onehundred');
   $('.dots').removeClass('onehundred finished active fifty');
   $('.choices.finished').removeClass('finished');
   $('.dot1').addClass('active');
   console.log(e.currentTarget);
 });
