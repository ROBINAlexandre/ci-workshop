$(document).ready(function($) {
  $('.text-here').hide()
  $('#ceb').click();
  var promoticker = function() {
    var window_width = window.innerWidth;
    var speed = 12 * window_width;
    $('#promo-notifications li:first').animate( {left: '-980px'}, speed, 'linear', function() {
      $(this).detach().appendTo('#promo-notifications ul').css('left', "100%");
      promoticker();
    });
  };
  if ($("#promo-notifications li").length > 1) {
    promoticker();
  }
    $('.modal').modal();
});
var loc = "<h1>BRAVO</h1>"
var arr= {loc:"Localisation",pro:"Programmes",res:"Ressources",inc:"Incubateur",infog:"Informations générales"}

$('.markHEP').on('click', function(){
  $('.text-here').hide()
  $('.'+this.id).show()
})

mapboxgl.accessToken = 'pk.eyJ1IjoiYWxleGFuZHJlcm9iaW5idHMiLCJhIjoiY2oycjdwYnplMDAwZjJ3bm91OWI5cmV5MCJ9.9byMyNKDd5lRKskEF1reFg';
var bounds = [
    [-1.57555,47.197373 ], // Southwest coordinates
    [-1.515691,47.218294]  // Northeast coordinates
];
var monument = [-1.5394009000000324, 47.2060207];
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v9',
    center: monument,
    maxBounds: bounds // Sets bounds as max
});

// create the popup
var popup = new mapboxgl.Popup({ offset: 25 })
    .setHTML('<h4>- CAMPUS HEP NANTES -</h4><p>16 Boulevard du Général de Gaulle</p><p>44200 NANTES</p>');

// create DOM element for the marker
var el = document.createElement('div');
el.id = 'marker';

// create the marker
new mapboxgl.Marker(el)
    .setLngLat(monument)
    .setPopup(popup) // sets a popup on this marker
    .addTo(map);
